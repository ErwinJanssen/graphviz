$oldPath = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path

$cmakeDir = "C:\Program Files\CMake\bin"
$msbuildDir = "C:\Program Files (x86)\MSBuild\14.0\Bin"
$perlDir = "C:\Program Files\Git\usr\bin"

$newPath = "$cmakeDir;$msbuildDir;$perlDir;$oldPath"

Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath

choco upgrade -y chocolatey
choco install -y cmake
choco upgrade -y --force visualstudio2015community -packageParameters "--Features NativeLanguageSupport_VCV1"
